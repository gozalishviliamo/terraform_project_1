region = "eu-central-1"

subnets = ["10.0.10.0/24", "10.0.20.0/24", "10.0.30.0/24"]

azones = [ "eu-central-1a", "eu-central-1b", "eu-central-1c" ]

ingress_rules = [ 22,80,443 ]
egress_rules = [ 22,80,443 ]

instance_type = "t2.micro"
ec2_name = ["production_ec2", "staging_ec2", "testing_ec2"]

web_traffic_name = "web security"

elastic_ip_name = ["production ip address", "staging ip address", "testing ip address"]

key_name = "aws_key_pair"
