variable "region" {
  type = string
  description = "default region"
  default = "eu-central-1"
}

variable "vpc_name" {
    type = string
    description = "vpc name"
    default = "DevOps VPC"
}

variable "vpc_cidr_block" {
  type = string
  description = "VPC ip range"
  default = "10.0.0.0/16"
}

# create list of subnets
variable "subnets" {
    type = list(string)
    description = "default list of subnets"
    default = ["10.0.10.0/24", "10.0.20.0/24", "10.0.30.0/24"]
}

# create list of availability zones
variable "azones" {
  type = list(string)
  description = "Availability VPC zones"
  default = [ "a", "b", "c" ]
}

# ingress/egress rules 
variable "ingress_rules" {
  type = list(number)
  default = [22,80,443]
}

variable "egress_rules" {
  type = list(number)
  default = [22,80,443]
}

###  AWS ec2 instance configuration vars
# Defining AMI
variable "ami" {
  default = {
    eu-central-1 = "ami-04e601abe3e1a910f"
  }
}

# Definign Key Name for connection
variable "key_name" {
  type = string
  description = "Desired name of AWS key pair"
  default = "aws_key_pair"
}

# Defining Instace Type
variable "instance_type" {
  type = string
  default = "t2.micro"
}

variable "ec2_name" {
  type = list(string)
  default = ["ec2_1", "ec2_2", "ec2_3"]
}
###

### WEB_traffic vars
variable "protocol" {
  type = string
  default = "TCP"
}

variable "cidr_block" {
  type = list(string)
  default = ["0.0.0.0/0"]
}

variable "web_traffic_name" {
  type = string
  default = "web security"
}
###

### Elastic ip (eip) vars
variable "domain" {
  type = string
  default = "vpc"
}
# tag name
variable "elastic_ip_name" {
  type = list(string)
  default = ["eip_1", "eip_2", "eip_3"]
}
###
