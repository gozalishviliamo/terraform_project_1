provider "aws" {
    region = var.region
}

terraform {
  backend "local" {
    path = "terraform.tfstate"
  }
}

# create new VPC
resource "aws_vpc" "DevOps_vpc" {
    cidr_block = var.vpc_cidr_block
    tags = {
        Name = var.vpc_name
    }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.DevOps_vpc.id
}

# create subnets and azones with iterator
resource "aws_subnet" "subnets" {
    count = length(var.azones)
    vpc_id = aws_vpc.DevOps_vpc.id
    cidr_block = element(var.subnets, count.index)
    availability_zone = element(var.azones, count.index)

    tags = {
        Name = element(var.azones, count.index)
    }
}

# ingress/egress rules
resource "aws_security_group" "web_traffic" {
    name = var.web_traffic_name
    vpc_id = aws_vpc.DevOps_vpc.id
    dynamic "ingress" {
        iterator = port
        for_each = var.ingress_rules
        content {
          from_port = port.value
          to_port = port.value
          protocol = var.protocol
          cidr_blocks = var.cidr_block
        }
    }

    dynamic "egress" {
        iterator = port
        for_each = var.egress_rules
        content {
          from_port = port.value
          to_port = port.value
          protocol = var.protocol
          cidr_blocks = var.cidr_block
        }
    }
}

resource "tls_private_key" "private_key" {
  algorithm   = "RSA"
  rsa_bits    = 2048
}

resource "aws_key_pair" "key_pair" {
  key_name   = var.key_name
  public_key = tls_private_key.private_key.public_key_openssh
}

resource "aws_instance" "ec2_instance" {
  count = length(var.azones)
  # for All instances will have the same ami and instance_type
  # AMI based on region 
  ami = lookup(var.ami, var.region)
  # Launching instance into subnet 
  subnet_id = aws_subnet.subnets[count.index].id
  # Instance type 
  instance_type = "${var.instance_type}"
  # SSH key which generated above for connection
  key_name = aws_key_pair.key_pair.key_name
  # Attaching security group to our instance
  security_groups = [aws_security_group.web_traffic.id]
  # Attaching Tag to Instance
  tags = {
    Name = element(var.ec2_name, count.index)
  }
}

# add elastic ip 
resource "aws_eip" "elastic_ip" {
  count = length(var.azones)
  domain = var.domain
  # count = length(var.ec2_name)
  instance = aws_instance.ec2_instance[count.index].id
  # associate_with_private_ip = "10.0.0.12"
  depends_on = [aws_internet_gateway.gw]

  tags = {
    Name = element(var.elastic_ip_name,count.index)
  }
}